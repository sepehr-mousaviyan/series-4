package sbu.cs.parser.json;

public class JsonBolean extends DataTypes {
    private Boolean value;
    public void setValue(Boolean value) {
        this.value = value;
    }
    @Override
    public String getvalue() {
        //for tests
        return Boolean.toString(this.value);
    }
}
