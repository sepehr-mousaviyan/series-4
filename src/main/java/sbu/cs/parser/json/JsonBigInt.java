package sbu.cs.parser.json;

public class JsonBigInt extends DataTypes{
    private Long value;
    public void setvalue(Long value) {
        this.value = value;
    }
    @Override
    public String getvalue() {
        //for tests
        return Long.toString(this.value);
    }
}
