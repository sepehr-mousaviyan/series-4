package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.List;

public class JsonArray extends DataTypes{
    private DataTypes [] Array;
    public JsonArray(DataTypes[] Array) {
        this.Array = Array;
    }
    public void setValue(DataTypes [] Array) {
        this.Array = Array;
    }
    @Override
    public String getvalue() {
        //for tests
        return null;
    }
    public static Json parse(String key, String StringArray) {
        List<DataTypes> JsonArrayList = new ArrayList<>();
        int start = 0;
        while(true) {
            int end = StringArray.indexOf(",",start);
            String block = StringArray.substring(start + 1, end);
            if(block.matches("True|False|true|false")) {
                JsonBolean objectValue = new JsonBolean();
                objectValue.setKey(key);
                objectValue.setValue(Boolean.parseBoolean(block));
                JsonArrayList.add(objectValue);
            }
            else if(block.matches("\\d*")) {
                JsonBigInt objectValue = new JsonBigInt();
                objectValue.setKey(key);
                objectValue.setvalue(Long.parseLong(block));
                JsonArrayList.add(objectValue);
            }
            else if(block.matches("\\d*\\.\\d*")) {
                JsonDouble objectValue = new JsonDouble();
                objectValue.setKey(key);
                objectValue.setValue(Double.parseDouble(block));
                JsonArrayList.add(objectValue);
            }
            else if(block.matches("[.*]")) {

            }
            else {
                String stringValue = block;
                if(block.matches("\".*\""))
                    stringValue = block.substring(1, block.length() - 1);
                JsonString objectValue = new JsonString();
                objectValue.setKey(key);
                objectValue.setValue(stringValue);
                JsonArrayList.add(objectValue);
            }
            start = ++end;
            if(start >= StringArray.length() - 1) {
                break;
            }
        }
        //for tests
        return new Json(JsonArrayList);
    }
}
