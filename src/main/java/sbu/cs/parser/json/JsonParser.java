package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.List;

public class JsonParser {

    /*
     * this function will get a String and returns a Json object
     */
    public static Json parse(String data) {
        List<DataTypes> JsonList = new ArrayList<>();
        int startPoint = 0;
        while(true) {
            //Find the key
            if(startPoint >= data.length() - 1) {
                break;
            }
            int startKey = data.indexOf("\"", startPoint);
            if(startKey == -1) {
                break;
            }
            int endKey = data.indexOf("\"", startKey + 1);
            String key = data.substring(startKey + 1, endKey).trim();
            //Find the value of that key
            int startValue = data.indexOf(":", endKey) + 1;
            int endValue = data.indexOf(",", startValue);
            int openBracketIndex = data.indexOf("[", startValue);
            if(0 <= openBracketIndex && openBracketIndex < endValue) {
                endValue = data.indexOf("]", startValue) + 1;
//                startValue--;
            }
            else if(endValue == -1) {
                endValue = data.indexOf("}", startValue);
            }
            String value = data.substring(startValue, endValue).trim();
//            System.out.println(value);
            startPoint = endValue;
            //Parse
            if(value.matches("True|False|true|false")) {
                JsonBolean objectValue = new JsonBolean();
                objectValue.setKey(key);
                objectValue.setValue(Boolean.parseBoolean(value));
                JsonList.add(objectValue);
            }
            else if(value.matches("\\d*")) {
                JsonBigInt objectValue = new JsonBigInt();
                objectValue.setKey(key);
                objectValue.setvalue(Long.parseLong(value));
                JsonList.add(objectValue);
            }
            else if(value.matches("\\d*\\.\\d*")) {
                JsonDouble objectValue = new JsonDouble();
                objectValue.setKey(key);
                objectValue.setValue(Double.parseDouble(value));
                JsonList.add(objectValue);
            }
            else {
                String stringValue = value;
                if(value.matches("\".*\""))
                    stringValue = value.substring(1, value.length() - 1);
                JsonString objectValue = new JsonString();
                objectValue.setKey(key);
                objectValue.setValue(stringValue);
                JsonList.add(objectValue);
            }
        }
        return new Json(JsonList);
    }

    /*
     * this function is the opposite of above function. implementing this has no score and
     * is only for those who want to practice more.
     */
    public static String toJsonString(Json data) {
        return null;
    }
}
