package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.List;

public class Json implements JsonInterface {
    List<DataTypes> JsonList = new ArrayList<>();
    public Json(List<DataTypes> JsonList)
    {
        this.JsonList = JsonList;
    }
    @Override
    public String getStringValue(String key) {
        for(DataTypes Element : JsonList) {
            if(Element.getkey().equals(key)) {
                return Element.getvalue();
            }
        }
        return null;
    }
}
