package sbu.cs.parser.json;

public class JsonDouble extends DataTypes{

    private double value;
    public void setValue(double value) {
        this.value = value;
    }
    @Override
    public String getvalue() {
        //for tests
        return Double.toString(this.value);
    }
}
